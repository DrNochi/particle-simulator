# Particle Simulator

This is a fun project I created about 2 years ago. It simulates thousands of particles that are attracted or repelled by different points in a 2D world. These particles are then rendered using SFML, which colors the particles differently depending on their distance to the mouse cursor. This can create beatiful patterns, if you play around a little bit.

## Development

1. This project has been developed for Windows using Visual Studio.
2. This project uses SFML 2.4.2 in a statically linked fashion. You therefore need to download SFML in a version compatible to 2.4.2.
3. After getting SFML, you need to set the additional include and library directories to the places where you installed SFML to.
4. You should be ready to go :)

To change this project to dynamically linking SFML follow these steps:
1. Adjust the linked libraries in the `lib.h` header file to use the dynamic ones.
2. Remove the `SFML_STATIC` macro definition.
3. Ensure that the respective `.dll`-files can be found.

*Although this project was not specifically developed with cross-platform capabilities in mind, it should be pretty easy to port over to other platforms as macOS and Linux as SFML is multi-platform capable.*
