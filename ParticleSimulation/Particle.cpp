#include "Particle.h"

Particle::Particle(sf::Vector2f position, sf::Vector2f speed) :
	m_position(position),
	m_speed(speed)
{

}

void Particle::Update(std::chrono::high_resolution_clock::duration deltaTime, sf::Vector2i worldSize, sf::Vector2f windowSize, std::vector<sf::Vector2i> attractorPositions, std::vector<float> attractorStrengths)
{
	sf::Vector2f deltaPosition(m_speed.x * cosf((float)deltaTime.count() / 1000000000 / 6.18f / 3), m_speed.y * cosf((float)deltaTime.count() / 1000000000 / 6.18f / 1));
	m_position += deltaPosition;

	if (m_position.x < -1.0f || m_position.x > worldSize.x)
	{
		//m_speed.x = -m_speed.x / 2;
		m_position.x = std::min(std::max(m_position.x, -1.0f), (float)worldSize.x);
		/*m_position.x = ((m_position.x / worldSize.x) - (int)(m_position.x / worldSize.x)) * worldSize.x;
		m_position.x += m_position.x < 0 ? worldSize.x : 0;*/
	}
	if (m_position.y < -1.0f || m_position.y > worldSize.y)
	{
		//m_speed.y = -m_speed.y / 2;
		m_position.y = std::min(std::max(m_position.y, -1.0f), (float)worldSize.y);
		/*m_position.y = ((m_position.y / worldSize.y) - (int)(m_position.y / worldSize.y)) * worldSize.y;
		m_position.y += m_position.y < 0 ? worldSize.y : 0;*/
	}

	//attractorPosition.x = worldSize.x / windowSize.x * attractorPosition.x;
	//attractorPosition.y = worldSize.y / windowSize.y * attractorPosition.y;

	for (int i = 0; i < attractorPositions.size(); ++i)
	{
		sf::Vector2f attratctorOffset = (sf::Vector2f)attractorPositions[i] - m_position;
		m_speed += attratctorOffset * (attractorStrengths[i] * (float)deltaTime.count() / 1000000000) * 1.0f / std::sqrt(std::pow(attratctorOffset.x, 2) + std::pow(attratctorOffset.y, 2));
	}
}

void Particle::UpdateRenderedParticle(sf::RenderWindow & window, sf::Vector2i worldSize, sf::Vector2i attractorPosition)
{
	sf::Vector2f windowSize = window.getView().getSize();
	m_renderedParticle = sf::RectangleShape(sf::Vector2f(windowSize.x / worldSize.x, windowSize.y / worldSize.y));

	sf::Vector2f rectanglePosition(windowSize.x / worldSize.x * m_position.x, windowSize.y / worldSize.y * m_position.y);
	m_renderedParticle.setPosition(rectanglePosition);

	sf::Vector2i rectangleOffset = (sf::Vector2i)rectanglePosition - attractorPosition;
	int colorOffset = std::min((int)(255 * std::sqrt(std::pow(rectangleOffset.x, 2.0) + std::pow(rectangleOffset.y, 2.0)) / std::min(windowSize.x, windowSize.y)), 255);
	m_renderedParticle.setFillColor(sf::Color(255 - colorOffset, colorOffset, 0, 255));
}

void Particle::Render(sf::RenderWindow &window)
{
	window.draw(m_renderedParticle);
}