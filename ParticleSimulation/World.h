#pragma once

#include <vector>
#include <chrono>
#include <SFML\Graphics.hpp>

#include "Particle.h"

class World
{
public:
	World(sf::Vector2i size);

	void AddParticles(int count);
	void DestroyParticles(int count);

	void Update(std::chrono::high_resolution_clock::duration deltaTime, sf::Vector2f windowSize);
	void Render(sf::RenderWindow &window);
	sf::Vector2i GetWorldSize() { return m_worldSize; };

	std::vector<sf::Vector2i> m_attractorPositions;
	std::vector<float> m_attractorStrengths;

	bool m_renderAttractors;

private:
	static void UpdateThreaded(World &world, int particleStartIndex, int particleEndIndex, std::chrono::high_resolution_clock::duration deltaTime, sf::Vector2f windowSize);
	static void RenderThreaded(World &world, int particleStartIndex, int particleEndIndex, sf::RenderWindow &window);

	sf::Vector2i m_worldSize;
	std::vector<Particle> m_particles;
};