#pragma once

#include <chrono>
#include <SFML\Graphics.hpp>

#include "World.h"

class Game
{
public:
	static int Run();

private:
	Game();

	void HandleEvent(sf::Event &event);
	void GameLoop(std::chrono::high_resolution_clock::duration deltaTime);

	sf::RenderWindow m_window;
	World m_world;

	bool m_cursorVisible;
};