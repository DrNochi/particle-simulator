#pragma once

#include <chrono>
#include <SFML\Graphics.hpp>

class Particle
{
public:
	Particle() = default;
	Particle(sf::Vector2f position, sf::Vector2f speed);

	void Update(std::chrono::high_resolution_clock::duration deltaTime, sf::Vector2i worldSize, sf::Vector2f windowSize, std::vector<sf::Vector2i> attractorPositions, std::vector<float> attractorStrengths);
	void UpdateRenderedParticle(sf::RenderWindow &window, sf::Vector2i worldSize, sf::Vector2i attractorPosition);
	void Render(sf::RenderWindow &window);

private:
	sf::Vector2f m_position;
	sf::Vector2f m_speed;

	sf::RectangleShape m_renderedParticle;
};