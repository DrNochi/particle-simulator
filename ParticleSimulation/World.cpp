#include "World.h"

#include <random>
#include <thread>
#include <iostream>

World::World(sf::Vector2i size) :
	m_worldSize(size),
	m_renderAttractors(true)
{
	std::random_device seed;
	std::mt19937 randomGenerator(seed());

	for (int i = 0; i < 10000; ++i)
	{
		const float speedSizeRatio = 10.0f;

		m_particles.push_back(Particle(
			sf::Vector2f(randomGenerator() % size.x, randomGenerator() % size.y),
			sf::Vector2f(randomGenerator() % size.x / speedSizeRatio - 0.5f * size.x / speedSizeRatio, randomGenerator() % size.y / speedSizeRatio - 0.5f * size.y / speedSizeRatio)
		));
	}

	m_attractorStrengths.push_back(100.0f);
	m_attractorPositions.push_back(m_worldSize / 2);
}

void World::AddParticles(int count)
{
	std::random_device seed;
	std::mt19937 randomGenerator(seed());

	for (int i = 0; i < count; ++i)
	{
		const float speedSizeRatio = 10.0f;

		m_particles.push_back(Particle(
			sf::Vector2f(randomGenerator() % m_worldSize.x, randomGenerator() % m_worldSize.x),
			sf::Vector2f(randomGenerator() % m_worldSize.x / speedSizeRatio - 0.5f * m_worldSize.x / speedSizeRatio, randomGenerator() % m_worldSize.y / speedSizeRatio - 0.5f * m_worldSize.y / speedSizeRatio)
		));
	}
}

void World::DestroyParticles(int count)
{
	int newCount = m_particles.size() - count;
	newCount = newCount < 0 ? 0 : newCount;

	m_particles.resize(newCount);
}

void World::Update(std::chrono::high_resolution_clock::duration deltaTime, sf::Vector2f windowSize)
{
	int numThreads = std::thread::hardware_concurrency();
	numThreads = 1;//numThreads ? numThreads : 1;

	std::vector<std::thread> threads;

	for (int i = 0; i < numThreads - 1; ++i)
		threads.push_back(std::thread(UpdateThreaded, std::ref(*this), i * m_particles.size() / numThreads, (i + 1) * m_particles.size() / numThreads - 1, deltaTime, windowSize));

	UpdateThreaded(*this, (numThreads - 1) * m_particles.size() / numThreads, m_particles.size() - 1, deltaTime, windowSize);

	for (auto &thread : threads)
		thread.join();

	//std::cout << "Update: " << deltaTime.count() << std::endl;
}

void World::Render(sf::RenderWindow &window)
{
	if (m_renderAttractors)
	{
		sf::Vector2f windowSize = window.getView().getSize();
		sf::CircleShape circle(std::min(windowSize.x / m_worldSize.x * 2, windowSize.y / m_worldSize.y * 2));

		for (auto attractorPosition : m_attractorPositions)
		{
			circle.setPosition(sf::Vector2f(attractorPosition.x * windowSize.x / m_worldSize.x, attractorPosition.y * windowSize.y / m_worldSize.y) - sf::Vector2f(circle.getRadius(), circle.getRadius()));
			circle.setFillColor(sf::Color(0, 0, 255, 255));
			window.draw(circle);
		}

		circle.setPosition(sf::Vector2f(m_attractorPositions[0].x * windowSize.x / m_worldSize.x, m_attractorPositions[0].y * windowSize.y / m_worldSize.y) - sf::Vector2f(circle.getRadius(), circle.getRadius()));
		circle.setFillColor(sf::Color(255, 0, 255, 255));
		window.draw(circle);
	}

	int numThreads = std::thread::hardware_concurrency();
	numThreads = 1;//numThreads ? numThreads : 1;

	std::vector<std::thread> threads;

	for (int i = 0; i < numThreads - 1; ++i)
		threads.push_back(std::thread(RenderThreaded, std::ref(*this), i * m_particles.size() / numThreads, (i + 1) * m_particles.size() / numThreads - 1, std::ref(window)));

	RenderThreaded(*this, (numThreads - 1) * m_particles.size() / numThreads, m_particles.size() - 1, window);

	for (auto &thread : threads)
		thread.join();

	for (auto &particle : m_particles)
		particle.Render(window);
}

void World::UpdateThreaded(World &world, int particleStartIndex, int particleEndIndex, std::chrono::high_resolution_clock::duration deltaTime, sf::Vector2f windowSize)
{
	for (int i = particleStartIndex; i <= particleEndIndex; ++i)
		world.m_particles[i].Update(deltaTime, world.m_worldSize, windowSize, world.m_attractorPositions, world.m_attractorStrengths);
}

void World::RenderThreaded(World &world, int particleStartIndex, int particleEndIndex, sf::RenderWindow &window)
{
	for (int i = particleStartIndex; i <= particleEndIndex; ++i)
		world.m_particles[i].UpdateRenderedParticle(window, world.m_worldSize, world.m_attractorPositions[0]);
}
