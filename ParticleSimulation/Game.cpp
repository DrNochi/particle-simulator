#include "Game.h"

#include <iostream>

int Game::Run()
{
	static Game game;

	std::cout << "Reset [N]" << std::endl;
	std::cout << "-------------------------------" << std::endl;
	std::cout << "Increase strength (10) [W]" << std::endl;
	std::cout << "Decrease strength (10) [S]" << std::endl;
	std::cout << "Increase strength (100) [Q]" << std::endl;
	std::cout << "Decrease strength (100) [A]" << std::endl;
	std::cout << "Set strength to 0 [D]" << std::endl;
	std::cout << "-------------------------------" << std::endl;
	std::cout << "Fix attractor [LMB]" << std::endl;
	std::cout << "Remove attractor [RMB]" << std::endl;
	std::cout << "-------------------------------" << std::endl;
	std::cout << "Add particles (1000) [Y]" << std::endl;
	std::cout << "Destroy particles (1000) [H]" << std::endl;
	std::cout << "Add particles (10000) [T]" << std::endl;
	std::cout << "Destroy particles (10000) [G]" << std::endl;
	std::cout << "-------------------------------" << std::endl;
	std::cout << "Toggle cursor visibility [C]" << std::endl;
	std::cout << "Toggle attractor visibility [X]" << std::endl;

	std::chrono::high_resolution_clock::duration gameTime = std::chrono::high_resolution_clock::now().time_since_epoch();
	while (game.m_window.isOpen())
	{
		sf::Event event;
		while (game.m_window.pollEvent(event))
			game.HandleEvent(event);

		std::chrono::high_resolution_clock::duration newGameTime = std::chrono::high_resolution_clock::now().time_since_epoch();
		game.GameLoop(newGameTime - gameTime);
		gameTime = newGameTime;
	}
	
	return 0;
}

Game::Game() :
	m_window(sf::VideoMode(1000, 1000), "Particle Simulator"),
	m_world(sf::Vector2i(1000, 1000)),
	m_cursorVisible(true)
{
	
}

void Game::HandleEvent(sf::Event &event)
{
	switch (event.type)
	{
	case sf::Event::Closed:
		m_window.close();
		break;
	
	case sf::Event::MouseMoved:
		m_world.m_attractorPositions[0] = sf::Vector2i(
			std::min((unsigned int)std::max(0, event.mouseMove.x), m_window.getSize().x) * m_world.GetWorldSize().x / m_window.getSize().x,
			std::min((unsigned int)std::max(0, event.mouseMove.y), m_window.getSize().y) * m_world.GetWorldSize().y / m_window.getSize().y
		);
		break;

	case sf::Event::MouseButtonPressed:
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			m_world.m_attractorPositions.push_back(m_world.m_attractorPositions[0]);
			m_world.m_attractorStrengths.push_back(m_world.m_attractorStrengths[0]);
		}
		else if (m_world.m_attractorPositions.size() > 1)
		{
				m_world.m_attractorPositions.pop_back();
				m_world.m_attractorStrengths.pop_back();
		}
		break;

	case sf::Event::KeyPressed:
		switch (event.key.code)
		{
		case sf::Keyboard::N:
			m_world = World(sf::Vector2i(1000, 1000));
			break;

		case sf::Keyboard::Q:
			m_world.m_attractorStrengths[0] += 100.0f;
			break;

		case sf::Keyboard::A:
			m_world.m_attractorStrengths[0] -= 100.0f;
			break;

		case sf::Keyboard::W:
			m_world.m_attractorStrengths[0] += 10.0f;
			break;

		case sf::Keyboard::S:
			m_world.m_attractorStrengths[0] -= 10.0f;
			break;

		case sf::Keyboard::D:
			m_world.m_attractorStrengths[0] = 0.0f;
			break;

		case sf::Keyboard::X:
			m_world.m_renderAttractors = !m_world.m_renderAttractors;
			break;

		case sf::Keyboard::C:
			m_window.setMouseCursorVisible(!m_cursorVisible);
			m_cursorVisible = !m_cursorVisible;
			break;

		case sf::Keyboard::T:
			m_world.AddParticles(10000);
			break;

		case sf::Keyboard::G:
			m_world.DestroyParticles(10000);
			break;

		case sf::Keyboard::Y:
			m_world.AddParticles(1000);
			break;

		case sf::Keyboard::H:
			m_world.DestroyParticles(1000);
			break;
		}
		break;
	}
}

void Game::GameLoop(std::chrono::high_resolution_clock::duration deltaTime)
{
		m_window.clear(sf::Color(0, 0, 32, 255));

		m_world.Update(deltaTime, m_window.getView().getSize());

		m_world.Render(m_window);

		m_window.display();
}
